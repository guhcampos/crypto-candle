# crypto-candle

## Notas

O projeto ficou um pouco maior do que eu esperava inicialmente, o que limitou a
extensão do que eu decidi implementar. As vítimas mais afetadas foram as de sempre:

  - Testes ficaram extremamente pobres, só escrevi alguns do serviço `aggregate`
  - Logs estão bastante *barebones* um mix de estruturado e não estruturado
  - Métricas não são geradas nas aplicações, e não foram implementados endpoints para o Prometheus
  - A performance não é das melhores

## Decisões de Implementação

### Moedas Escolhidas

Para escolher as moedas mais relevantes, criei uma Task que gera um report das
10 mais negociadas a cada 10 segundos. Após algumas observações e queries no
Google, decidi escolher as cotações de `BTC`, `ETC` e `ETH`, todas indexadas em
`USDT` a fim de tornar comparações visuais mais simples.

### Versionamento

Eu não sou particularmente fã de semantic versioning para aplicações com continuous
delivery: acho oneroso ter que raciocinar sobre a semântica de um release quando se
faz múltiplos releases em uma semana ou mesmo num dia. Por isso acabei adotando em
meus projetos o padrão YYYYMMDD-SHORTSHA. Muitos adotam o SHA1 do commit apenas, mas
eu gosto de ter as versões cronologicamente ordenáveis, o que torna toda sorte de
script e automações muito mais simples.

### Redis

Assim como vocês, também sou fã do Redis. Como conversamos sobre o Redis Streams
recentemente, decidi experimentá-lo e notei que boa parte das features que uso
com frequência no Kafka estão presentes (em particular: Consumer Groups) e acabei
adotando a solução.

~~A escolha do Elasticsearch no lugar de um banco SQL é meramente questão de gosto:
como estou tratando todas as mensagens passadas como documentos JSON, era mais
simples adotar um banco documental do que se preocupar com SQL, ORM e afins. Além
disso, lembro que mencionaram problemas de escalabilidade do banco de vocês, e o
Elasticsearch escala que é uma beleza, com mínimo esforço.~~

A persistência do dado no Elasticsearch acabou estourando o tempo que eu havia
reservado para me dedicar ao projeto (20 horas) e decidi que, apesar de constar
claramente nos requisitos que os dados devem ser persistidos num banco, e concordar
que o Redis não pode ser considerado um banco de dados, esse seria um requisito
secundário haja vista que o dado histórico jamais seria consultado no nosso escopo.

Caso isso seja um problema, há implementações mais triviais que podem ser feitas
adotando MongoDB ou SQLAlchemy e um database relacional. O que eu faria então
seria implementar uma nova classe `PersistedCandleMixin` e usá-la em conjunto com a
`RedisCandle` - renomeada para `RedisCandleMixin` para compor um objeto capaz de
ser gravado em ambos os backing stores.

### Sem Pandas?

Pois é, muita coisa seria mais fácil (tipo renderizar a tabela do cliente) se eu
tivesse adotado o Pandas desde o início e transformado tudo em dataframes, mas
quando comecei a escrever o código, achei que seria overkill. Acabou que não foi,
mas como eu já estava quase no fim quando notei, ficou tudo feito a mão mesmo =).

### Configuração

Toda a configuração é feita através de variáveis de ambiente prefixadas com `CC`,
a fim de mitigar colisões com outros softwares. Os padrões são propositalmente
valores usados em desenvolvimento como `localhost` e não devem funcionar caso
vazem para a produção. A ideia aqui é *fail fast*: a aplicação não pode subir com
um valor default inválido, sob pena de acessar o banco incorreto, por exemplo. Como
estamos fazendo deploy no Kubernetes, a própria infra nos garante que tráfego não
será direcionado para um `Pod` com falha: a revisão anterior continua funcionando
até que a versão nova esteja estável.

### Sync x Async

Vocês vão notar que nos serviços `collect` e `feed` eu adoto uma estratégia de
programação assíncrona, enquanto o `aggregate` é síncrono. Em geral, prefiro manter
o código síncrono sempre que possível porque é mais fácil de entender, corrigir e
observar. Nos dois casos citados o `async` foi usado para possibilitar que o mesmo
serviço monitore um stream (ou websocket) enquanto provê updates para o usuário (ou
o stream seguinte) sem precisar de polling.

### Microsserviços

Apesar de separado em serviços distintos, o projeto compartilha a gestão de
dependências via Pipfile. Inicialmente eu havia criado um diretório completo
com dependências distintas para cada um, mas honestamente isso faria o CI/CD
mais complicado do que faz sentido para a atividade.

#### Uma nota sobre duplicação de código

Há bastante código duplicado entre os serviços, como a boilerplate de logging,
que poderia ser evitado extraindo-se esse código numa biblioteca. Se fosse um
projeto de longa duração isso seria justificado, mas num projeto *ad-hoc* como
esse o CTRL+C/CTRL+V entre projetos é mais rápido =).

### CI/CD

Apesar de o Gitlab-CI funcionar perfeitamente para builds de Docker, fazer um push
das imagens do Gitlab para o Google Cloud, ou fazer o Pull de umage imagem do Gitlab
Registry via Google Cloud são ambas opções chatas de se fazer. Minhas experiências
anteriores me fizeram optar pelo Cloud Build que também é gratuito e mais fácil de
se autenticar via Gitlab.

Já em termos de Deploy, decidi não adotar as ferramentas que costumo preferir:
ArgoCD ou GoCD, porque precisaria fazer o deploy das ferramentas em si, que não
estão disponíveis no meu projeto pessoal do Google Cloud. Como alternativa,
realizo um deployment direto via `kubectl`, o que torna inviável fazer rolling
upgrades, canary e blue/green em tempo hábil.
