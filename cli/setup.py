from setuptools import setup, find_packages

setup(name="cc_cli", packages=find_packages(), entry_points={"console_scripts": ["cc-cli = cc_cli.app:main",]})
