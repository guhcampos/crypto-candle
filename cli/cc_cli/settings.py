import os
from dotenv import load_dotenv

load_dotenv()

CC_WEBSOCKET_URL = os.environ.get("CC_WEBSOCKET_URL", "ws://localhost:8080/ws")
