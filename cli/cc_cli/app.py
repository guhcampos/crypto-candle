import asyncio
import curses
import aioredis
import aiohttp
import structlog
import json
import arrow
from humanfriendly.tables import format_pretty_table
import collections
import itertools
from .settings import CC_WEBSOCKET_URL
import sys

log = structlog.getLogger(__name__)

Subscription = collections.namedtuple("Subscription", ["currency", "interval"])

def update_table(table, msg):

    update = json.loads(msg.data)

    index = (update["currency"],update["interval"])

    if index not in table:
        table[index] = {}

    table[index] = [
        arrow.get(update["data"]["timestamp"]),
        update["data"]["high"],
        update["data"]["low"],
        update["data"]["open"],
        update["data"]["close"]
    ]


    for key in table:
        row = [key[0]]
        row.extend([key[1]])
        row.extend(table[key])
        yield row


async def main():
    """
    Conecta-se ao Websocket do Crypto Candle e recebe atualizações inscritas,
    mostrando-as na tela.
    """

    session = aiohttp.ClientSession()

    subscriptions = [
        ("USDT-ETC", "1"),
        ("USDT-BTC", "5"),
        ("USDT-ETH", "10")
    ]

    print("inscrevendo-se em {}".format(subscriptions))

    try:
        async with session.ws_connect(CC_WEBSOCKET_URL) as ws:

            print("conectando-se ao websocket crypto-candle")

            # Confirmando que a mensagem de ACK de conexão da Polionex é válida.
            connect_ok = await ws.receive()

            if connect_ok.type != aiohttp.WSMsgType.TEXT:
                raise ConnectionError("Houve um problema na conexão com o Websocket: {}".format(connect_ok))

            await ws.send_str(json.dumps(subscriptions))

            subscribe_ok = await ws.receive()

            if subscribe_ok.type != aiohttp.WSMsgType.TEXT:
                raise ConnectionError("Houve um problema ao inscrever-se no canal de tickers: {}".format(subscribe_ok))

            print("conexão estabelecida, escutando mensagens")

            headers = [
                "moeda",
                "intervalo",
                "datetime",
                "open",
                "low",
                "high",
                "close"
            ]
            table = {}
            async for msg in ws:
                sys.stdout.write("\r{}".format(format_pretty_table(sorted(update_table(table, msg)), headers)))
                sys.stdout.flush()

            print("conexão encerrada")

    except Exception as e:
        ws.exception()
        print(e)
        raise

    finally:
        await session.close()

asyncio.run(main())
