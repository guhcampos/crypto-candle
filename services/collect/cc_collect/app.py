import asyncio
import collections
import json

import aiohttp
import aioredis
import structlog

from .settings import POLIONEX_WEBSOCKET_URL, REDIS_CONNECT_STRING, REPORT_SLEEP_INTERVAL, REPORT_TOP_COINS_COUNT

log = structlog.getLogger(__name__)


CURRENCIES = {121: "USDT_BTC", 149: "USDT_ETH", 173: "USDT_ETC"}
POLIONEX_SUBSCRIBE_COMMAND = {"command": "subscribe", "channel": 1002}
POLIONEX_TICKER_CHANNEL_ID = 1002
REDIS_STREAM_NAME = "collect:{id}"

monitored_ids = [currency[1] for currency in CURRENCIES.items()]

tick_counter = collections.Counter()


TickUpdate = collections.namedtuple(
    "TickUpdate",
    [
        "id",
        "last_trade_price",
        "lowest_ask",
        "highest_bid",
        "percent_change_24h",
        "volume_24h",
        "quote_24h",
        "is_frozen",
        "highest_price_24h",
        "lowest_price_24h",
    ],
)


async def task_report():
    """
    Usado como uma Task, loga periodicamente os trades mais comuns.
    """
    while True:
        rank = tick_counter.most_common(REPORT_TOP_COINS_COUNT)
        if rank:
            log.info({"message": "TOP_CURRENCIES_REPORT", "ranking": rank})
        await asyncio.sleep(REPORT_SLEEP_INTERVAL)


async def parse_and_forward_message(msg, redis):
    """
    Decodifica uma mensagem do Websocket Polionex.
    """
    data = json.loads(msg.data)
    if data[0] == POLIONEX_TICKER_CHANNEL_ID:

        tick_counter.update([data[2][0]])

        # ignoramos todos os updates de moedas que não monitoramos, e enriquecemos
        # os que estamos interessados com o timestamp atual e o identificador
        # da moeda em formato string
        if data[2][0] in CURRENCIES:
            data[2][0] = CURRENCIES[data[2][0]]

            log.debug({"message": "UPDATE_RECEIVED", "data": data})
            parsed = TickUpdate(*data[2])

            await redis.xadd(REDIS_STREAM_NAME.format(id=parsed.id), {"price": parsed.last_trade_price})

            log.debug({"message": "UPDATE_SENT", "ticket": parsed.id, "price": parsed.last_trade_price})


async def main():
    """
    Conecta-se ao Websocket da Polionex e inscreve-se no channel dos tickers,
    recebendo todas as atualizações enviadas.
    """
    report_task = asyncio.create_task(task_report())
    session = aiohttp.ClientSession()

    async with session.ws_connect(POLIONEX_WEBSOCKET_URL) as ws:

        log.info("conectando-se ao websocket polionex")

        # Confirmando que a mensagem de ACK de conexão da Polionex é válida.
        connect_ok = await ws.receive()

        if connect_ok.type != aiohttp.WSMsgType.TEXT:
            raise ConnectionError("Houve um problema na conexão com o Websocket Polionex: {}".format(connect_ok))

        await ws.send_str(json.dumps(POLIONEX_SUBSCRIBE_COMMAND))

        # Confirmando que a mensagem de ACK de subscribe da Polionex é válida.
        log.info("monitorando moedas: [%s]", ",".join([str(k) for k in CURRENCIES]))

        subscribe_ok = await ws.receive()

        if subscribe_ok.type != aiohttp.WSMsgType.TEXT:
            raise ConnectionError("Houve um problema ao inscrever-se no canal de tickers: {}".format(subscribe_ok))

        # Daqui passamos o resto da vida recebendo e enfileirando mensagens
        redis = await aioredis.create_redis(REDIS_CONNECT_STRING, encoding="utf-8")

        async for msg in ws:
            await parse_and_forward_message(msg, redis=redis)


asyncio.run(main())
