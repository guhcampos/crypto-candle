import collections
import os

from dotenv import load_dotenv

load_dotenv()

Currency = collections.namedtuple("Currency", ["ticker", "id"])

POLIONEX_WEBSOCKET_URL = os.environ.get("CC_POLIONEX_WEBSOCKET_URL", "wss://api2.poloniex.com")
REPORT_SLEEP_INTERVAL = int(os.environ.get("CC_REPORT_SLEEP_INTERVAL", 10))
REPORT_TOP_COINS_COUNT = int(os.environ.get("CC_REPORT_TOP_COINS_COUNT", 10))
REDIS_CONNECT_STRING = os.environ.get("CC_REDIS_CONNECT_STRING", "redis://localhost")
