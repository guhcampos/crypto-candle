from setuptools import setup, find_packages

setup(
    name="cc_collect",
    packages=find_packages(),
    entry_points={"console_scripts": ["cc-collect=cc_collect.app:main",]},
)
