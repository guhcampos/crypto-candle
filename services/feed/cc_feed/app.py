import asyncio
import aiohttp
import aiohttp.web
import structlog
import aioredis
import collections
import json
import logging
from .settings import CC_REDIS_CONNECT_STRING

log = logging.getLogger(__name__)

router = aiohttp.web.RouteTableDef()

REDIS_CHANNEL_NAME = "channel:{id}:{interval}"

Subscription = collections.namedtuple("Subscription", ["currency", "interval"])

def process_message(msg):
    """
    Processa cada mensagem individualmente, enviando para o socket.
    """

    _, currency, interval = msg[1][0].decode().split(":")

    return json.dumps({
        "currency": currency,
        "interval": interval,
        "data": json.loads(msg[1][1])
    })

@router.get("/ws")
async def websocket_handler(request):
    """
    Inicializa uma conexão websocket
    """

    ws = aiohttp.web.WebSocketResponse()
    receiver = aioredis.pubsub.Receiver()
    redis = await aioredis.create_redis(CC_REDIS_CONNECT_STRING, encoding="utf-8")

    await ws.prepare(request)
    await ws.send_str(json.dumps({"status": "ok"}))

    # protocolo de inicio de conexao
    msg = await ws.receive()

    if msg.type == aiohttp.WSMsgType.TEXT:
        subscriptions = json.loads(msg.data)
        assert isinstance(subscriptions, list)
    else:
        log.error("ws protocol error, message is not text")
        return ws.exception()
        return

    # inscricao nos channels solicitados
    try:
        for sub in subscriptions:

            subscription = Subscription(*sub)

            channel = REDIS_CHANNEL_NAME.format(id=subscription.currency, interval=subscription.interval)
            await redis.subscribe(receiver.channel(channel))
            await redis.psubscribe(receiver.pattern('*'))
            log.info("subscribed to channel {}".format(channel))

            async for msg in receiver.iter():
                log.debug({"message": "MESSAGE_SENT {}".format(msg[1])})
                await ws.send_str(process_message(msg))

    except Exception as e:
        log.error(e)
        return ws.exception()

    await ws.send_str(json.dumps({"status": "done"}))

    log.warn("websocket closed by the client")


def main():
    app = aiohttp.web.Application()
    app.add_routes(router)
    aiohttp.web.run_app(app)

if __name__ == "__main__":
    main()
