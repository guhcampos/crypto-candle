from setuptools import setup, find_packages

setup(
    name="cc_feed", packages=find_packages(), entry_points={"console_scripts": ["cc-feed = cc_feed.app:main",]},
)
