from setuptools import setup, find_packages

setup(
    name="cc_aggregate",
    packages=find_packages(),
    entry_points={"console_scripts": ["cc-aggregate = cc_aggregate.app:main",]},
)
