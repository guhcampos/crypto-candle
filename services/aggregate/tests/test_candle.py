import arrow
import pytest

from cc_aggregate.candle import Candle
from cc_aggregate.candle import InvalidCandleError


def test_candle_init():

    data = {"low": "1.0", "high": "2.0", "close": "1.5", "open": "1.5"}

    candle = Candle(**data)
    assert candle.low == data["low"]
    assert candle.high == data["high"]
    assert candle.close == data["close"]
    assert candle.open == data["open"]

    with pytest.raises(InvalidCandleError):
        bad_data = data.copy()
        bad_data["low"] = "2.1"
        candle = Candle(**bad_data)


def test_candle_update():

    data = {"low": "1.0", "high": "2.0", "close": "1.5", "open": "1.5"}

    candle = Candle(**data)

    candle.update("0.5")
    assert candle.low == "0.5"
    assert candle.open == "1.5"
    assert candle.close == "0.5"

    candle.update("5.3")
    assert candle.low == "0.5"
    assert candle.open == "1.5"
    assert candle.high == "5.3"
    assert candle.close == "5.3"
