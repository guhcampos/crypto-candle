import arrow
import pytest
import redis

from cc_aggregate.candle import RedisCandle
from cc_aggregate.candle import InvalidCandleError


def test_rediscandle():
    """
    Obs.: esse teste pode falhar se por acaso for rodado exatamente no limiar
    dos 1000 minutos.
    """
    rd = redis.Redis(host="localhost", encoding="utf-8", decode_responses=True)

    rcandle = RedisCandle(currency="USDT_BTC", price="23.45", redis=rd, interval=1000)
    assert rcandle.candle.close == "23.45"

    rcandle = RedisCandle(currency="USDT_BTC", price="54.32", redis=rd, interval=1000)
    assert rcandle.candle.open == "23.45"
    assert rcandle.candle.close == "54.32"
    assert rcandle.candle.high == "54.32"
    assert rcandle.candle.low == "23.45"
