import os
from dotenv import load_dotenv

load_dotenv()

REDIS_CONNECT_STRING = os.environ.get("CC_REDIS_CONNECT_STRING", "redis://localhost")
