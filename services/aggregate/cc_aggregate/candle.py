import decimal
import arrow
import json
import logging

class InvalidCandleError(ValueError):
    pass


class Candle:

    log = logging.getLogger(__name__)


    def __init__(self, **kwargs):
        self.high = kwargs.pop("high")
        self.low = kwargs.pop("low")
        self.open = kwargs.pop("open")
        self.close = kwargs.pop("close")

        if self.low > self.high:
            raise InvalidCandleError("low > high")

    def update(self, price):
        """
        A partir de um dicionário representando um candle, faz as atualizações
        necessárias e retorna o candle atualizado.
        """

        dec_price = decimal.Decimal(price)
        dec_low = decimal.Decimal(self.low)
        dec_high = decimal.Decimal(self.high)

        if dec_price < dec_low:
            self.low = str(dec_price)

        if dec_price > dec_high:
            self.high = str(dec_price)

        self.close = price


class RedisCandle:

    REDIS_KEY_NAME = "{id}:{date}"
    REDIS_CHANNEL_NAME = "channel:{id}:{interval}"
    REDIS_EXPIRE_MULTIPLIER = 10 * 60

    log = logging.getLogger(__name__)


    def __init__(self, **kwargs):
        self.price = kwargs.pop("price")
        self.interval = kwargs.pop("interval")
        self.redis = kwargs.pop("redis")
        self.currency = kwargs.pop("currency")

        one_minute = arrow.utcnow().floor("minute")

        self.key = self.REDIS_KEY_NAME.format(id=self.currency, date=str(one_minute.shift(minutes=-(one_minute.minute % self.interval))))
        self.channel = self.REDIS_CHANNEL_NAME.format(id=self.currency, interval=self.interval)

        cached = self.redis.hgetall(self.key)
        if cached:
            self.candle = Candle(**cached)

        if not cached:
            self.candle = Candle(**{"high": self.price, "low": self.price, "open": self.price, "close": self.price})

        self.candle.update(self.price)
        self.redis.hmset(self.key, self.candle.__dict__)
        self.redis.expire(self.key, self.interval * self.REDIS_EXPIRE_MULTIPLIER)

    def publish(self):
        self.log.debug("PUBLISH_UPDATE {} -> {}".format(self.channel, self.__str__()))
        return self.redis.publish(self.channel, json.dumps(self.data()))

    def data(self):
        return {
            "timestamp": arrow.utcnow().timestamp,
            "open": self.candle.open,
            "close": self.candle.close,
            "low": self.candle.low,
            "high": self.candle.high
        }

    def __str__(self):
        return str(self.data())
