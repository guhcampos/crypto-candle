import decimal
import uuid

import redis
# import structlog

from .candle import RedisCandle
from .settings import REDIS_CONNECT_STRING

import logging
log = logging.getLogger(__name__)

decimal.getcontext().prec = 8

REDIS_INPUT_STREAM_NAME = "collect:{id}"
REDIS_GROUP_ID = "aggregate"
REDIS_HOST = REDIS_CONNECT_STRING.split(":")[1].split("/")[2]  # gambiarra para compatibilizar com aioredis
WORKER_ID = str(uuid.uuid4())


def compute_all_candles(msg, rd):
    """
    Cria e publica todos os candles individualmente.
    """

    stream = msg[0]
    msgid = msg[1][0][0]
    price = msg[1][0][1]["price"]

    candle_one = RedisCandle(currency=stream.split(":")[1], price=price, redis=rd, interval=1)
    candle_five = RedisCandle(currency=stream.split(":")[1], price=price, redis=rd, interval=5)
    candle_ten = RedisCandle(currency=stream.split(":")[1], price=price, redis=rd, interval=10)

    log.info({
        "message": "UPDATED_CANDLES",
        "data": (candle_one, candle_five, candle_ten)
    })

    candle_one.publish()
    candle_five.publish()
    candle_ten.publish()

    return stream, msgid


def process_messages():
    """
    Itera eternamente sobre todas as mensagens recebidas no Consumer Group do
    Redis streams.
    """
    rd = redis.Redis(host=REDIS_HOST, encoding="utf-8", decode_responses=True)

    streams = rd.keys("collect:*")

    for stream in streams:
        try:
            rd.xgroup_create(stream, REDIS_GROUP_ID)
        except redis.exceptions.ResponseError:
            log.info("grupo %s/%s existe, prosseguindo..", stream, REDIS_GROUP_ID)
            continue

    while True:
        # lemos uma mensagem de cada vez, o que pode não ser eficiente a
        # princípio mas deixa o código mais legível.
        batch = rd.xreadgroup(REDIS_GROUP_ID, WORKER_ID, {stream: ">" for stream in streams}, count=1)
        for msg in batch:
            stream, msgid = compute_all_candles(msg, rd=rd)
            # log.debug({"message": "CANDLE_COMPUTED", "data": candle})
            rd.xack(stream, REDIS_GROUP_ID, msgid)

def main():
    """
    Precisamos de uma main() para habilitar o entrypoint do setup.py
    """
    log.info("Iniciando agregador")
    process_messages()


if __name__ == "__main__":
    main()
