FROM python:3.7

ENV PYTHONDONTWRITEBYTECODE 1
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

RUN apt update && apt install python3.7-dev libffi-dev -y
RUN curl --silent https://bootstrap.pypa.io/get-pip.py | python3.7
RUN pip3 install pipenv

RUN mkdir -p /crypto-candle/services/feed/
WORKDIR /crypto-candle

COPY Pipfile Pipfile.lock /crypto-candle/
RUN pipenv install --deploy

COPY services/feed /crypto-candle/services/feed/
RUN pipenv run pip install services/feed

ENV CC_REDIS_CONNECT_STRING "redis://redis"

CMD ["pipenv", "run", "cc-feed"]
